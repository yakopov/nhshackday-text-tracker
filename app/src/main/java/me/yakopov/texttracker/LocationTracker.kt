package me.yakopov.texttracker

import android.location.LocationListener
import android.location.LocationManager
import android.location.Location

import android.content.Context

import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast

class LocationTracker (private val mContext: Context, internal val smsBroadcastReceiver: SmsBroadcastReceiver) {

    private val locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    private fun displayLocationDetectedToast() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
        if (prefs.getBoolean("toast", false)) {
            Toast.makeText(mContext, "Location detected, sending it back...", Toast.LENGTH_LONG).show()
        }
    }

    private var locationListenerGPS: LocationListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            // found location - need to report the result...
            displayLocationDetectedToast()
            smsBroadcastReceiver.sendLocation(location)
            // ...and stop the further location updates
            locationManager.removeUpdates(this)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        }

        override fun onProviderEnabled(provider: String) {
        }

        override fun onProviderDisabled(provider: String) {
        }
    }

    fun readLocation() {
        try {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    2000,
                    10.toFloat(),
                    locationListenerGPS
            )
        } catch (e: SecurityException) {
            // TODO: smarter checks for the lack of permission and disabled location services
            // is needed here (user might be suggested to enable them etc.)
        }
    }
}