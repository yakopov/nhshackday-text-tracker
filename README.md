Text Location Tracker
=====================

A quick project for
[NHS Hack Day](https://nhshackday.com) of June 30-July 01 2018: an Android application that listens to text messages coming
from pre-defined phone number with a certain keyword in them, and replies back with another
text message containing phone's current coordinates.

**Slides**:
* [Saturday pitch](https://twitter.com/y_akopov/status/1012999849543905280)
* [Sunday presentation](https://docs.google.com/presentation/d/15MbwoDgmYn_WS2dThFfB9lRr4I-6osTkiDCpFUoPhUU/edit?usp=sharing)

APK (uses lazy permissions in the Manifest so might not work in higher Android versions):
[link in the snippet description](https://gitlab.com/snippets/1729576)

## Building

Open the project in Android Studio with Kotlin support (if you have just downloaded it, Kotlin
is supported by default).

Use Studio Tools / SDK Manager to download Android SDK (I was targeting Android 5.0 which is API 21). You can use more than
one as the Studio allows to specify min version of the API separately.

You will also need JDK (I used 1.8). Kotlin SDK is included with the Studio.

To build a release APK, you will need to sign it, so here is how to generate your key:

`keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -validity 10000`

## Resources

This project was built with with next to no knowledge of Kotlin and no experience of native Android development
(which was another reason to do it in order to learn the basics of both).

Predictably, I was googling heavily, so here are the links that were the most useful:

**Dealing with text messages:**
* [Detecting and sending SMS on Android](https://android.jlelse.eu/detecting-sending-sms-on-android-8a154562597f)
by Joaquim Ley
* [SAM convertion in Kotlin vs. Java explained](!https://stackoverflow.com/a/43737962)
* **Also:**
    * [Check if incoming SMS is from favorite contact](https://discuss.kotlinlang.org/t/check-if-incoming-sms-is-from-favorite-contact/7282)

**Dealing with GPS:**
* [Getting location offline](https://stackoverflow.com/a/46906215) at StackOverflow
* **Also:**
    * [AndroidCodility/GPS-Location](https://github.com/AndroidCodility/) by Govind Rastogi

**Workflow:**
* [Starting Activities with Kotlin](https://medium.com/@passsy/starting-activities-with-kotlin-my-journey-8b7307f1e460) by passsy
* [You won’t believe this one weird trick to handle Android Intent extras with Kotlin](!https://medium.com/@workingkills/you-wont-believe-this-one-weird-trick-to-handle-android-intent-extras-with-kotlin-845ecf09e0e9) by Eugenio Marletti

**Build:**
* [Signing an Android Application for Real Life Mobile Device Usage / Installation](http://www.androiddevelopment.org/tag/apk/)

## Questions?

Feel free to [email](mailto:akopov@hotmail.co.uk) or [tweet](https://twitter.com/y_akopov) me.