package me.yakopov.texttracker

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceActivity
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.content.IntentFilter
import android.provider.Telephony
import android.widget.Toast

/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 * See [Android Design: Settings](http://developer.android.com/design/patterns/settings.html)
 * for design guidelines and the [Settings API Guide](http://developer.android.com/guide/topics/ui/settings.html)
 * for more information on developing a Settings UI.
 */
class SettingsActivity : AppCompatPreferenceActivity() {

    // is going to be null if we are currently tracking the messages
    private var lastSmsBroadcastReceiver: SmsBroadcastReceiver? = null

    /**
     * Subscribes to new text messages
     */
    private fun startListeningToSms() {
        stopListeningToSms()

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)

        val requestNumber = prefs.getString("request_number", "")
        val requestSecret = prefs.getString("request_secret", "")
        val trackingEnabled = prefs.getBoolean("tracking", true)

        val smsBroadcastReceiver = SmsBroadcastReceiver(requestNumber, requestSecret, trackingEnabled)
        registerReceiver(smsBroadcastReceiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))
        lastSmsBroadcastReceiver = smsBroadcastReceiver

        // TODO: do we need to check if the previous location tracker (for the previous broadcaster) is still alive?
        val locationTracker = LocationTracker(this, smsBroadcastReceiver)

        val listener = object : SmsBroadcastReceiver.Listener {
            override fun onTextReceived(text: String) {
                locationTracker.readLocation()
            }
        }

        smsBroadcastReceiver.setListener(listener)

        // Toast.makeText(this, "Registered listener", Toast.LENGTH_SHORT).show()
    }

    /**
     * Stops listening to new text messages
     */
    private fun stopListeningToSms(): Boolean {
        if (lastSmsBroadcastReceiver != null) {
            unregisterReceiver(lastSmsBroadcastReceiver)
            lastSmsBroadcastReceiver = null

            // Toast.makeText(this, "Unregistered listener", Toast.LENGTH_SHORT).show()
            return true
        }

        // Toast.makeText(this, "Nothing to unregister", Toast.LENGTH_SHORT).show()

        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()

        startListeningToSms()
    }

    override fun onDestroy() {
        super.onDestroy()

        stopListeningToSms()
    }

    /**
     * Set up the [android.app.ActionBar], if the API is available.
     */
    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * {@inheritDoc}
     */
    override fun onIsMultiPane(): Boolean {
        return isXLargeTablet(this)
    }

    /**
     * {@inheritDoc}
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    override fun onBuildHeaders(target: List<PreferenceActivity.Header>) {
        loadHeadersFromResource(R.xml.pref_headers, target)
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String): Boolean {
        return PreferenceFragment::class.java.name == fragmentName
                || GeneralPreferenceFragment::class.java.name == fragmentName
                || NotificationPreferenceFragment::class.java.name == fragmentName
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    class GeneralPreferenceFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.pref_general)
            setHasOptionsMenu(true)

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("request_number"))
            bindPreferenceSummaryToValue(findPreference("request_secret"))

            bindPreferenceSwitch(findPreference("tracking"))
        }
    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    class NotificationPreferenceFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.pref_notification)
            setHasOptionsMenu(true)
        }
    }

    companion object {

        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
            val activity = preference.context as SettingsActivity

            if (preference.key == "request_number") {
                activity.lastSmsBroadcastReceiver?.requestNumber = value.toString()
            } else if (preference.key == "request_secret") {
                activity.lastSmsBroadcastReceiver?.requestSecret = value.toString()
            }

            preference.summary = value.toString()

            true
        }

        private val sBindPreferenceSwitchListener = Preference.OnPreferenceChangeListener { preference, value ->
            var activity = preference.context as SettingsActivity
            activity.lastSmsBroadcastReceiver?.enabled = (value == true)

            true
        }

        /**
         * Helper method to determine if the device has an extra-large screen. For
         * example, 10" tablets are extra-large.
         */
        private fun isXLargeTablet(context: Context): Boolean {
            return context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_XLARGE
        }

        /**
         * Binds a preference's summary to its value. More specifically, when the
         * preference's value is changed, its summary (line of text below the
         * preference title) is updated to reflect the value. The summary is also
         * immediately updated upon calling this method. The exact display format is
         * dependent on the type of preference.

         * @see .sBindPreferenceSummaryToValueListener
         */
        private fun bindPreferenceSummaryToValue(preference: Preference) {
            // Set the listener to watch for value changes.
            preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getString(preference.key, ""))
        }

        private fun bindPreferenceSwitch(preference: Preference) {
            preference.onPreferenceChangeListener = sBindPreferenceSwitchListener

            sBindPreferenceSwitchListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getBoolean(preference.key, false))
        }
    }
}
