package me.yakopov.texttracker

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.provider.Telephony
import android.telephony.SmsManager
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.Date

/**
 * TODO:
 *
 * Initial idea was to keep the number and secret immutable and to unregister/re-register broadcaster
 * when they change. However, that doesn't work well, I cannot yet guarantee there is only going to be
 * 1 or 0 listeners only, and that they are going to be up to date. Yet to get this right, so
 * going with a single but mutable version now
 */
class SmsBroadcastReceiver(var requestNumber: String, var requestSecret: String, var enabled: Boolean) : BroadcastReceiver() {

    private var listener: Listener? = null

    /**
     * Triggered by the new text message arriving in default system app
     */
    override fun onReceive(context: Context, intent: Intent) {
        if (enabled) {
            if (intent.action == Telephony.Sms.Intents.SMS_RECEIVED_ACTION) {
                for (smsMessage in Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                    val smsSender = smsMessage.displayOriginatingAddress

                    if (smsSender == requestNumber) {
                        if ((requestSecret == "") or (requestSecret == smsMessage.messageBody)) {
                            if (listener != null) {
                                listener!!.onTextReceived(smsMessage.messageBody)
                            }
                        }
                    }
                }
            }
        }
    }

    internal fun setListener(listener: Listener) {
        this.listener = listener
    }

    internal interface Listener {
        fun onTextReceived(text: String)
    }

    /**
     * Sends provided location back in a text message
     */
    fun sendLocation(location: Location) {
        val latitude = location.latitude
        val longitude = location.longitude

        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val currentDate = sdf.format(Date())
        val text = "$currentDate\n\nLan: $latitude Lon: $longitude\n\nhttps://www.google.com/maps/place/$latitude,$longitude"

        SmsManager.getDefault().sendTextMessage(requestNumber, null, text, null, null)
    }

    companion object {
        private val TAG = "SmsBroadcastReceiver"
    }
}
